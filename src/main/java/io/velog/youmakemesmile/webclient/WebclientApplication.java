package io.velog.youmakemesmile.webclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.List;

@SpringBootApplication
@RestController
@Slf4j
public class WebclientApplication {

    private final WebClient sampleWebClient;

    public WebclientApplication(@Qualifier("sampleWebClient") WebClient sampleWebClient) {
        this.sampleWebClient = sampleWebClient;
    }


    public static void main(String[] args) {
        SpringApplication.run(WebclientApplication.class, args);
    }

    @GetMapping("/callee/get/path-variable/{pathVariable1}/{pathVariable2}")
    public ResponseEntity<String> calleeJson(
            @PathVariable String pathVariable1,
            @PathVariable BigDecimal pathVariable2,
            @RequestParam(required = false) String requestParam1,
            @RequestParam(required = false) BigDecimal requestParam2) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        for (Enumeration<?> e = servletRequestAttributes.getRequest().getHeaderNames(); e.hasMoreElements(); ) {
            String nextHeaderName = (String) e.nextElement();
            String headerValue = servletRequestAttributes.getRequest().getHeader(nextHeaderName);
            log.info(nextHeaderName + headerValue);
        }
        return ResponseEntity
                .ok()
                .header("callee", "callee")
                .body(servletRequestAttributes.getRequest().getRequestURI() + "?" + servletRequestAttributes.getRequest().getQueryString());
    }

    @GetMapping("/caller/get/path-variable")
    public String callerJson() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        return sampleWebClient
                .get()
                .uri("/callee/get/path-variable", uriBuilder ->
                        uriBuilder.path("/{pathVariable1}/{pathVariable2}")
                                .queryParam("requestParam1", "requestParam1")
                                .queryParam("requestParam2", 2)
                                .build("pathVariable1", 1))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<String>() {
                })
                .block();
    }

    @GetMapping("/callee/get/image/{id}")
    public ResponseEntity<byte[]> calleeJson(
            @PathVariable String id) throws IOException {
        ClassPathResource resource = new ClassPathResource("한글명.png");
        InputStreamResource inputStreamResource = new InputStreamResource(resource.getInputStream(), "test.png");
        ContentDisposition build = ContentDisposition.attachment().filename(resource.getFilename(), Charset.defaultCharset()).build();
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, build.toString())
                .body(inputStreamResource.getInputStream().readAllBytes());
    }

    @GetMapping("/caller/get/image/{id}")
    public ResponseEntity<byte[]> image() throws IOException {
//        InputStreamResource block = sampleWebClient.get()
//                .uri("/callee/get/image/{id}", 1)
//                .accept(MediaType.APPLICATION_OCTET_STREAM)
//                .retrieve()
//                .bodyToMono(new ParameterizedTypeReference<InputStreamResource>() {
//                })
//                .block();
//        return ResponseEntity
//                .ok()
//                .contentType(MediaTypeFactory.getMediaType(block.getFilename()).get())
//                .body(block.getInputStream().readAllBytes());
        ResponseEntity<byte[]> block = sampleWebClient.get()
                .uri("/callee/get/image/{id}", 1)
                .accept(MediaType.APPLICATION_OCTET_STREAM)
                .exchangeToMono(value -> value.toEntity(new ParameterizedTypeReference<byte[]>() {
                }))
                .block();

        List<String> strings = block.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION);
        ContentDisposition parse = ContentDisposition.parse(strings.get(0));
        return ResponseEntity
                .ok()
                .contentType(MediaTypeFactory.getMediaType(parse.getFilename()).get())
                .body(block.getBody());
    }


    @PostMapping("/callee/post/json/{id}")
    public String calleePostJson(@RequestBody String str) {
        return str;
    }


    @PostMapping("/caller/post/json/{id}")
    public String callerPostJson() {
        return sampleWebClient.post()
                .uri("/callee/post/json/{id}", 1)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue("TEST!!!"))
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    @PostMapping("/callee/post/files/{id}")
    public void calleePostFiles(@RequestParam MultipartFile[] files) throws IOException {
        for (MultipartFile multipartFile : files) {
            FileOutputStream fileOutputStream = new FileOutputStream(new File("./" + multipartFile.getOriginalFilename()));
            fileOutputStream.write(multipartFile.getBytes());
        }
    }

    @PostMapping("/caller/post/files/{id}")
    public void callerPostFiles() throws FileNotFoundException {
        MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("files", new InputStreamResource(new FileInputStream("./arrow-down1.png"))).filename("한글명.png");
        multipartBodyBuilder.part("files", new ClassPathResource("한글명.png")).filename("한글명11.png");
        sampleWebClient
                .post()
                .uri("/callee/post/files/{id}",1)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(multipartBodyBuilder.build()))
                .retrieve()
                .toBodilessEntity()
                .block();
    }


}
